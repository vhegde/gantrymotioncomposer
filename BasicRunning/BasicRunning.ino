boolean setdirx=HIGH;//LOW is left
boolean setdiry=LOW;//LOW is closer to us
boolean setdirz=LOW;// LOW is the down direction

boolean movex=HIGH;
boolean movey=LOW;
boolean movez=LOW;

int checkloopx=0;
int checkloopy=0;
int checkloopzUP=0;
int checkloopzDN=0;

int spdx=500;
int spdy=500;
int spdz=1000;

int inPinEstop = 8;    // Estop connected to digital pin 8
int inPinLimSW = 9;    // Limit SWs connected to digital pin 9
int val = 0;      // variable to store the read value

void setup() {
  DDRD = B11111111;
  /*   This port is used to control both the direction
   *   and the pulse of 4 separate motors.
   *   bit 0: direction of  z-axis
   *   bit 1: pulse of      z-axis
   *   bit 2: direction of  y2-axis
   *   bit 3: pulse of      y2-axis
   *   bit 4: direction for y1-axis
   *   bit 5: pulse for     y1-axis
   *   bit 6: direction for x-axis
   *   bit 7: pulse for     x-axis
   */
   pinMode(inPinEstop,INPUT);//to Estop
   pinMode(inPinLimSW,INPUT);//to Lim SW
   Serial.begin(38400);
}

void revmotorx(){
  setdirx=!setdirx;
  delayMicroseconds(10000);
}

void revmotory(){
  setdiry=!setdiry;
  delayMicroseconds(10000);
}

void revmotorz(){
  setdirz=!setdirz;
  delayMicroseconds(10000);
}

void loop() {
  //Serial.println(digitalRead(inPinEstop));
  Serial.println(digitalRead(inPinLimSW)); 
  //Serial.println("Hello"); 
  if(!(digitalRead(inPinEstop))) return;
  if((digitalRead(inPinLimSW))) return;
  
  if( movex ){
    if( checkloopx >= 1000 ){
      checkloopx=0;
      delayMicroseconds(500000);
      revmotorx();
    }
    checkloopx++;
    // pulse...
    if( setdirx ){
      PORTD = B11000000;
      delayMicroseconds(spdx);
      PORTD = B01000000;
      delayMicroseconds(spdx);
    }else{
      PORTD = B10000000;
      delayMicroseconds(spdx);
      PORTD = B00000000;
      delayMicroseconds(spdx);
    }
  }

  if( movey ){
    if( checkloopy >= 1000 ){
      checkloopy=0;
      delayMicroseconds(500000);
      revmotory();
    }
    checkloopy++;
    // pulse...
    if( setdiry ){
      PORTD = B00111100;
      delayMicroseconds(spdy);
      PORTD = B00010100;
      delayMicroseconds(spdy);
    }else{
      PORTD = B00101000;
      delayMicroseconds(spdy);
      PORTD = B00000000;
      delayMicroseconds(spdy);
    }
  }

  if( movez ){
    if( checkloopzUP >= 1000 ){
      checkloopzUP=0;
      delayMicroseconds(500000);
      revmotorz();
    }

    if( checkloopzDN >= 1000 ){
      checkloopzDN=0;
      delayMicroseconds(500000);
      revmotorz();
    }
    if(setdirz){
      checkloopzUP++;
    }else{
      checkloopzDN++;
    }

    // pulse...
    if( setdirz ){
      PORTD = B00000011;
      delayMicroseconds(spdz);
      PORTD = B00000001;
      delayMicroseconds(spdz);
    }else{
      PORTD = B00000010;
      delayMicroseconds(spdz);
      PORTD = B00000000;
      delayMicroseconds(spdz);
    }
  }
}
