boolean setdirx=HIGH;//LOW is left
boolean setdiry=HIGH;//LOW is closer to us
boolean setdirz=LOW;// LOW is the down direction

long XstepsToMove=0;
long YstepsToMove=0;
long ZstepsToMove=0;
long stepsPerSec=0;
long maxX = 238000;// max value is 240863;
long minY = -159000;// min value is -161811;
int minZ = -6500;// min value is -6600;

long Xpos=0;
long Ypos=0;
long Zpos=0;

String command, str1, retMessage;

int val = 0, nEstopMsg = 0;      // variable to store the read value
int analogPinX = A0, analogPinYL = A1, analogPinYR = A2, analogPinZ = A3;
int analogValX = -1, analogValYL = -1, analogValYR = -1, analogValZ = -1;
int analogLowVal = 200, analogHighVal = 700;//for 10 bit ADC, high is 1024. When LimSW are engaged, value is HIGH
int inPinEstop = 13;    // Estop connected to digital pin 13
int homeSpeedDelay = 200; //delay in micoSec for homing XY
int homeSpeedDelayZ = 1000; //delay in micoSec for homing Z
int nStepsBackWhenLimSW = 1000;// no. of stpes to come back after lim SW is disengaged.

void setup() {
  DDRD = B11111111;
  /*   This port is used to control both the direction
   *   and the pulse of 4 separate motors.
   *   bit 0: Not used
   *   bit 1: not used
   *   bit 2: direction of  y2-axis
   *   bit 3: pulse of      y2-axis
   *   bit 4: direction for y1-axis
   *   bit 5: pulse for     y1-axis
   *   bit 6: direction for x-axis
   *   bit 7: pulse for     x-axis
   */
  DDRB = B00000011;
  /*  bit 0: direction of  z-axis //pin 8
   *   bit 1: pulse of      z-axis //pin 9
   *   bit 2 - 4 : not used
   *   bit 5 : Estop read
   *   bit 6 & 7: cannot be used
   */
  Serial.begin(250000);
  while (!Serial){;}// Wait for serial to connect
  Serial.setTimeout(1);
  pinMode(inPinEstop,INPUT);//to Estop
}

bool expMotionInRange(long x, long y, long z){
  bool inRange = true;
  if(x > maxX) {retMessage += "E-031 "; inRange = false;}
  if(x < 0)    {retMessage += "E-021 "; inRange = false;}
  
  if(y > 0)    {retMessage += "E-032 "; inRange = false;}
  if(y < minY) {retMessage += "E-022 "; inRange = false;}
  
  if(z > 0)    {retMessage += "E-034 "; inRange = false;}
  if(z < minZ) {retMessage += "E-024 "; inRange = false;}

  return inRange;
}

void recoverXLimSW(){
  while(analogRead(analogPinX) > analogHighVal){
    if(setdirx){//X+ caused limit SW. So move X-
      PORTD = B10000000;
      delayMicroseconds(homeSpeedDelay);
      PORTD = B00000000;
      delayMicroseconds(homeSpeedDelay);
      Xpos--;
    }
    else{//X- caused limit SW. So move X+
      PORTD = B11000000;
      delayMicroseconds(homeSpeedDelay);
      PORTD = B01000000;
      delayMicroseconds(homeSpeedDelay);
      Xpos++;
    }
  }
  for(int i=0;i<nStepsBackWhenLimSW;i++){
    if(setdirx){//X+ caused limit SW. So move X- nStepsBack times
      PORTD = B10000000;
      delayMicroseconds(homeSpeedDelay);
      PORTD = B00000000;
      delayMicroseconds(homeSpeedDelay);
      Xpos--;
    }
    else{//X- caused limit SW. So move X+ nStepsBack times
      PORTD = B11000000;
      delayMicroseconds(homeSpeedDelay);
      PORTD = B01000000;
      delayMicroseconds(homeSpeedDelay);
      Xpos++;
    }
  }
}

void recoverYLimSW(){
  while(analogRead(analogPinYL) > analogHighVal || analogRead(analogPinYR) > analogHighVal){
    if(setdiry){//Y+ caused limit SW. So move Y-
      PORTD = B00101000;
      delayMicroseconds(homeSpeedDelay);
      PORTD = B00000000;
      delayMicroseconds(homeSpeedDelay);
      Ypos--;
    }
    else{//Y- caused limit SW. So move Y+
      PORTD = B00111100;
      delayMicroseconds(homeSpeedDelay);
      PORTD = B00010100;
      delayMicroseconds(homeSpeedDelay);
      Ypos++;
    }
  }
  for(int i=0;i<nStepsBackWhenLimSW;i++){
    if(setdiry){//Y+ caused limit SW. So move Y- nStepsBack times
      PORTD = B00101000;
      delayMicroseconds(homeSpeedDelay);
      PORTD = B00000000;
      delayMicroseconds(homeSpeedDelay);
      Ypos--;
    }
    else{//Y- caused limit SW. So move Y+ nStepsBack times
      PORTD = B00111100;
      delayMicroseconds(homeSpeedDelay);
      PORTD = B00010100;
      delayMicroseconds(homeSpeedDelay);
      Ypos++;
    }
  }
}

void recoverZLimSW(){
  while(analogRead(analogPinZ) > analogHighVal){
    if(setdirz){//Z+ caused limit SW. So move Z-. There is no SW for lower end. So this is the only condition.
      PORTB = B00000010;
      delayMicroseconds(homeSpeedDelayZ);
      PORTB = B00000000;
      delayMicroseconds(homeSpeedDelayZ);
      Zpos--;
    }
    else{//Z- caused limit SW. So move Z+
      PORTB = B00000011;
      delayMicroseconds(homeSpeedDelayZ);
      PORTB = B00000001;
      delayMicroseconds(homeSpeedDelayZ);
      Zpos++;
    }
  }
  for(int i=0;i<nStepsBackWhenLimSW/10;i++){
    if(setdirz){//Z+ caused limit SW. So move Z- nStepsBack/10 times
      PORTB = B00000010;
      delayMicroseconds(homeSpeedDelayZ);
      PORTB = B00000000;
      delayMicroseconds(homeSpeedDelayZ);
      Zpos--;
    }
    else{//Z- caused limit SW. So move Z+ nStepsBack/10 times
      PORTB = B00000011;
      delayMicroseconds(homeSpeedDelayZ);
      PORTB = B00000001;
      delayMicroseconds(homeSpeedDelayZ);
      Zpos++;
    }
  }
}
void movexy(){
  while(XstepsToMove != 0 && YstepsToMove != 0){//X & Y
    if(analogRead(analogPinX) > analogHighVal)  {retMessage += "E-011 ";recoverXLimSW(); XstepsToMove = 0;}
    if(analogRead(analogPinYL) > analogHighVal) {retMessage += "E-012 ";recoverYLimSW(); YstepsToMove = 0;}
    if(analogRead(analogPinYR) > analogHighVal) {retMessage += "E-013 ";recoverYLimSW(); YstepsToMove = 0;}
    if(!(digitalRead(inPinEstop))){  XstepsToMove = 0; YstepsToMove = 0; retMessage += "E-999 "; break;}
  
    if(XstepsToMove > 0 && YstepsToMove > 0){//X+ and Y+
      PORTD = B11111100;
      delayMicroseconds(stepsPerSec);
      PORTD = B01010100;
      delayMicroseconds(stepsPerSec);
      XstepsToMove--;
      YstepsToMove--;
      Xpos++;
      Ypos++;
    }
    else if(XstepsToMove > 0 && YstepsToMove < 0){//X+ and Y-
      PORTD = B11101000;
      delayMicroseconds(stepsPerSec);
      PORTD = B01000000;
      delayMicroseconds(stepsPerSec);
      XstepsToMove--;
      YstepsToMove++;
      Xpos++;
      Ypos--;
    }
    else if(XstepsToMove < 0 && YstepsToMove > 0){//X- and Y+     
      PORTD = B10111100;
      delayMicroseconds(stepsPerSec);
      PORTD = B00010100;
      delayMicroseconds(stepsPerSec);
      XstepsToMove++;
      YstepsToMove--;
      Xpos--;
      Ypos++;
    }      
    else if(XstepsToMove < 0 && YstepsToMove < 0){//X- and Y-     
      PORTD = B10101000;
      delayMicroseconds(stepsPerSec);
      PORTD = B00000000;
      delayMicroseconds(stepsPerSec);
      XstepsToMove++;
      YstepsToMove++;
      Xpos--;
      Ypos--;
    } 
  }//X & Y end
}
void movex(){
  while(XstepsToMove != 0){//X only
    if(analogRead(analogPinX) > analogHighVal)  {retMessage += "E-011 "; recoverXLimSW(); XstepsToMove = 0;}
    if(!(digitalRead(inPinEstop))){ XstepsToMove = 0; retMessage += "E-999 "; break;}
    if(XstepsToMove > 0){//X+
      PORTD = B11000000;
      delayMicroseconds(stepsPerSec);
      PORTD = B01000000;
      delayMicroseconds(stepsPerSec);
      XstepsToMove--;
      Xpos++;
    }
    else if(XstepsToMove < 0){//X-
      PORTD = B10000000;
      delayMicroseconds(stepsPerSec);
      PORTD = B00000000;
      delayMicroseconds(stepsPerSec);
      XstepsToMove++;
      Xpos--;
    }
  }//X only end
}

void movey(){
  while(YstepsToMove != 0){//Y only
    if(analogRead(analogPinYL) > analogHighVal) {retMessage += "E-012 ";recoverYLimSW(); YstepsToMove = 0;}
    if(analogRead(analogPinYR) > analogHighVal) {retMessage += "E-013 ";recoverYLimSW(); YstepsToMove = 0;}
    if(!(digitalRead(inPinEstop))){ YstepsToMove = 0; retMessage += "E-999 "; break;}
    if(YstepsToMove > 0){//Y+
      PORTD = B00111100;
      delayMicroseconds(stepsPerSec);
      PORTD = B00010100;
      delayMicroseconds(stepsPerSec);
      YstepsToMove--;
      Ypos++;
    }
    else if(YstepsToMove < 0){//Y-
      PORTD = B00101000;
      delayMicroseconds(stepsPerSec);
      PORTD = B00000000;
      delayMicroseconds(stepsPerSec);
      YstepsToMove++;
      Ypos--;
    }
  }//Y only end    
}

void movez(){
  while( ZstepsToMove !=0 ){
    if(analogRead(analogPinZ) > analogHighVal)  {retMessage += "E-014 "; recoverZLimSW(); ZstepsToMove = 0;}
    if(!(digitalRead(inPinEstop))){ ZstepsToMove = 0; retMessage += "E-999 "; break;}
    if(ZstepsToMove > 0){//Z+
      PORTB = B00000011;
      delayMicroseconds(stepsPerSec*30);
      PORTB = B00000001;
      delayMicroseconds(stepsPerSec*30);
      ZstepsToMove--;
      Zpos++;
    }
    else if(ZstepsToMove < 0){//Z-
      PORTB = B00000010;
      delayMicroseconds(stepsPerSec*30);
      PORTB = B00000000;
      delayMicroseconds(stepsPerSec*30);
      ZstepsToMove++;
      Zpos--;
    }
  }//Z only end
}

void homeGantry(){
  stepsPerSec = homeSpeedDelayZ*30;
  if(ZstepsToMove != 0) movez();
  stepsPerSec = homeSpeedDelay;
  if(XstepsToMove != 0 && YstepsToMove != 0) movexy();
  if(XstepsToMove != 0) movex();
  if(YstepsToMove != 0) movey();
}

void loop() {
  if(!(digitalRead(inPinEstop))){
    XstepsToMove = 0; YstepsToMove = 0; ZstepsToMove = 0;
    if(nEstopMsg < 2){ Serial.print("E-999 D-000"); nEstopMsg++;}
    return;
  }
 
  analogValX  = analogRead(analogPinX);
  analogValYL = analogRead(analogPinYL);
  analogValYR = analogRead(analogPinYR);
  analogValZ  = analogRead(analogPinZ);

  //Serial.print("X: "+(String)analogValX + " YL: "+(String)analogValYL + " YR: "+(String)analogValYR + " Z: "+(String)analogValZ);
  //if(analogValX > analogHighVal || analogValYL > analogHighVal || analogValYR > analogHighVal || analogValZ > analogHighVal) return;
  //delay(1000);
  retMessage = "";
  if(analogValX > analogHighVal)  {retMessage += "E-001 "; XstepsToMove = 0;}
  if(analogValYL > analogHighVal) {retMessage += "E-002 "; YstepsToMove = 0;}
  if(analogValYR > analogHighVal) {retMessage += "E-003 "; YstepsToMove = 0;}
  if(analogValZ > analogHighVal)  {retMessage += "E-004 "; ZstepsToMove = -100;}

  while (!Serial.available());
  //if( Serial.available() == 32){
  command = Serial.readString();
  //Serial.print(command);
  //if(retMessage.startsWith("E-")){Serial.print(retMessage);}

  if(command.equals("HOME")){
    XstepsToMove = -250000;
    YstepsToMove = 167000;
    ZstepsToMove = 6800;
    stepsPerSec = homeSpeedDelay;
    setdirx = LOW;
    setdiry = HIGH;
    setdirz = HIGH;
    homeGantry();
    Xpos = 0;
    Ypos = 0;
    Zpos = 0;
    retMessage += "HOMED. ";
  }
  else if(command.equals("GETPOSITION")){
    //retMessage += (String)Xpos+","+(String)Ypos+","+(String)Zpos+","+(String)stepsPerSec+" ";
    retMessage += "["+(String)Xpos+","+(String)Ypos+","+(String)Zpos+","+(String)stepsPerSec+"]";
    //Serial.print(retMessage);
  }
  else if(command.equals("LIMSWSTATUS")){
    retMessage += " |X:"+(String)(analogValX > analogHighVal)+" YL:"+(String)(analogValYL > analogHighVal)+" YR:"+(String)(analogValYR > analogHighVal)+" Z:"+(String)(analogValZ > analogHighVal)+"|";
  }
  else{
    str1 = command.substring(command.indexOf("X")+1,command.indexOf("Y")); XstepsToMove = str1.toInt();
    str1 = command.substring(command.indexOf("Y")+1,command.indexOf("Z")); YstepsToMove = str1.toInt();
    str1 = command.substring(command.indexOf("Z")+1,command.indexOf("F")); ZstepsToMove = str1.toInt();
    str1 = command.substring(command.indexOf("F")+1,command.indexOf("_")); stepsPerSec = str1.toInt();

    if( command.startsWith("G90") ){ //if abs position to move to
      XstepsToMove = XstepsToMove - Xpos;
      YstepsToMove = YstepsToMove - Ypos;
      ZstepsToMove = ZstepsToMove - Zpos;
    }

    if(expMotionInRange(Xpos+XstepsToMove, Ypos+YstepsToMove , Zpos+ZstepsToMove)){
      //retMessage = "X="+(String)XstepsToMove+" Y="+(String)YstepsToMove+" Z="+(String)ZstepsToMove+" F="+(String)stepsPerSec;
      //Serial.print(retMessage);

      if(XstepsToMove > 0) setdirx = HIGH;
      else setdirx = LOW;

      if(YstepsToMove > 0) setdiry = HIGH;
      else setdiry = LOW;

      if(ZstepsToMove > 0) setdirz = HIGH;
      else setdirz = LOW;
    
      if(XstepsToMove != 0 && YstepsToMove != 0) movexy();
      if(XstepsToMove != 0) movex();
      if(YstepsToMove != 0) movey();
      if(ZstepsToMove != 0) movez();
    }
    //Serial.print("LocXYZ:"+(String)Xpos+" "+(String)Ypos+" "+(String)Zpos);
    retMessage += "["+(String)Xpos+","+(String)Ypos+","+(String)Zpos+","+(String)stepsPerSec+"]";
  }
  Serial.print(retMessage+" D-000"); //Done executing motion
  //  }// Serial.available()
  command="";
}
