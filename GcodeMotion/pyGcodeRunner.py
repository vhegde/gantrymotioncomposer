# Takes Gcode as input in the format "G1X0Y0Z0F0", where 0 can be replaced by an integer.
# Enter "Gend" to exit Gcode interface (not case sensitive).

import serial
import time
arduino = serial.Serial(port='/dev/ttyACM1', baudrate=250000, timeout=0.05)

timeNeededToMove = 1
absMotion = False
Xval = 0
Yval = 0
Zval = 0
Fval = 500
xPos = 0.
yPos = 0.
zPos = 0.
xstepsToMove = 0
ystepsToMove = 0
zstepsToMove = 0
stepsPerSec = 0

def getnumInBetween(lstr,startChar,endChar):
    idxStart = lstr.find(startChar) + 1
    idxEnd = lstr.find(endChar,idxStart)
    lstr = lstr[idxStart:idxEnd]
    return float(lstr)
def convert_mm_to_steps(idx,num1):
    convrFac = [1.0, 1.0, 1.0, 1.0] # X, Y , Z and F
    return round(num1*convrFac[idx]) #steps per mm = 1.0
def getXYZF(locStr):
    global xPos, yPos, zPos
    if ('[' in locStr) and (']' in locStr):
        idxStart = locStr.find('[') + 1
        idxEnd   = locStr.find(']',idxStart)
        locStr = locStr[idxStart:idxEnd]
        val = [float(i) for i in locStr.split(',')]
        xPos = val[0]
        yPos = val[1]
        zPos = val[2]
        Fval = val[3]
        return
    else:
        xPos = -100.0
        yPos = 100.0
        zPos = 100.0
        print("E-501: Expected [ and ] in arduino return string. Not found") 
        return
    
def interpretGcode(Gstr):
    global absMotion, Xval, Yval, Zval, Fval
    Gstr = Gstr.upper()
    if (Gstr == "HOME") : return "HOME"
    elif (Gstr == "GETPOSITION" or Gstr == "GPS"): return "GETPOSITION"
    elif (Gstr == "LIMSWSTATUS"): return "LIMSWSTATUS"
    elif (Gstr == ".G"): return ".G"
    elif (Gstr == "?"):
        print("AbsMotion? "+str(absMotion))
        return ""
    elif (Gstr == ""): return ""

    if 'G90' in Gstr:
        absMotion = True
    elif 'G91' in Gstr:
        absMotion = False
    elif ( ('X' not in Gstr) and ('Y' not in Gstr) and ('Z' not in Gstr)):
        print("Not a valid command")
        return ""
    
    Xval = 0
    Yval = 0
    Zval = 0
    Gstr = Gstr.replace('G90', '')
    Gstr = Gstr.replace('G91', '')
    #print("Initial:",str, absMotion)
    Gstr = Gstr.replace('G1', '')
    Gstr = Gstr.replace('X', '_X')
    Gstr = Gstr.replace('Y', '_Y')
    Gstr = Gstr.replace('Z', '_Z')
    Gstr = Gstr.replace('F', '_F')
    Gstr = Gstr + '_'
    #print(str)
    if( 'X' in Gstr): Xval = convert_mm_to_steps(0, getnumInBetween(Gstr, 'X', '_'))
    elif (absMotion): Xval = xPos
    else: Xval = 0
    
    if( 'Y' in Gstr): Yval = convert_mm_to_steps(1, getnumInBetween(Gstr, 'Y', '_'))
    elif (absMotion): Yval = yPos
    else: Yval = 0
    
    if( 'Z' in Gstr): Zval = convert_mm_to_steps(2, getnumInBetween(Gstr, 'Z', '_'))
    elif (absMotion): Zval = zPos
    else: Zval = 0
    
    if( 'F' in Gstr): Fval = convert_mm_to_steps(3, getnumInBetween(Gstr, 'F', '_'))
    if(Fval<10): Fval = 10
    # print(Xval,Yval,Zval,Fval)
    if (absMotion): return 'G90G1X' + str(Xval) + 'Y' + str(Yval) + 'Z' + str(Zval) + 'F' + str(Fval)
    else: return 'G1X' + str(Xval) + 'Y' + str(Yval) + 'Z' + str(Zval) + 'F' + str(Fval)
    #return (Xval, Yval, Zval, Fval)

#print(interpretGcode(input("")))


def write_read(cmd0):
    arduino.flushOutput()
    arduino.write(bytes(cmd0, 'utf-8'))
    while True: # wait for command execution
        dataRec = arduino.readline()
        if 'D-000' in str(dataRec):
            print(dataRec)
            arduino.flushInput()
            break
        time.sleep(0.05)
    return str(dataRec)

def readWriteArduino():
    dataRec = arduino.readline()
    if 'E-' in str(dataRec):
        print(dataRec)

    while True:
        # arduino.flushOutput()
        cmd0 = input("G> ") # Taking input from user
        cmd0 = interpretGcode(cmd0)
        if cmd0 == "": continue
        if cmd0==".G":
            break
        write_read(cmd0)
        getXYZF(write_read("GETPOSITION"))
        # arduino.write(bytes(cmd0, 'utf-8'))
        # while True: # wait for command execution
        #     dataRec = arduino.readline()
        #     if 'D-000' in str(dataRec):
        #         print(dataRec)
        #         arduino.flushInput()
        #         break
        #     time.sleep(0.05)

getXYZF(write_read("GETPOSITION")) # get the position first
readWriteArduino()

