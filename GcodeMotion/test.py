# Importing Libraries
import serial
import time
import sys

port_name = '/dev/ttyACM0'
if len(sys.argv) > 1 :
    port_name = sys.argv[1]

arduino = serial.Serial(port=port_name, baudrate=250000, timeout=0.05)

""" This code transmits messagae to the arduino controling 
the gantry.  A user should pass a string to the funct 
with moveY, moveX, or moveZ.  These should then be 
followed by a number, which can be either positive or 
negative.  
e.g.: 

write_read("moveX10") -- move ten steps to the right
moveY-10 -- move tenb steps forward
moveZ-10 -- move ten steps down
"""

def write_read(x):
    while len(x) < 32 : 
        x = x[:] + "_"
    # data = arduino.readline()
    # print(data)
    arduino.write(bytes(x, 'utf-8'))
    time.sleep(0.05)
    arduino.flushOutput()
    data = arduino.readline()
    time.sleep(0.05)
    # data = arduino.readline()
    # time.sleep(0.05)
    arduino.flushInput()
    return data

