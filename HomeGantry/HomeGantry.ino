boolean setdirx=LOW;//LOW is left
boolean setdiry=HIGH;//LOW is closer to us
boolean setdirz=HIGH;// LOW is the down direction

boolean movex=1;
boolean movey=1;
boolean movez=1;

int movedX=0;
int movedY=0;
int movedZ=0;

int checkloopx=0;
int checkloopy=0;
int checkloopzUP=0;
int checkloopzDN=0;

int spdx=100;
int spdy=100;
int spdz=1000;

int inPinEstop = 13;    // Estop connected to digital pin 13

int val = 0;      // variable to store the read value
int analogPinX = A0, analogPinYL = A1, analogPinYR = A2, analogPinZ = A3;
int analogValX = -1, analogValYL = -1, analogValYR = -1, analogValZ = -1;
int analogLowVal = 200, analogHighVal = 700;//for 10 bit ADC, high is 1024

void setup() {
  DDRD = B11111111;
  /*   This port is used to control both the direction
   *   and the pulse of 4 separate motors.
   *   bit 0: Not used
   *   bit 1: not used
   *   bit 2: direction of  y2-axis
   *   bit 3: pulse of      y2-axis
   *   bit 4: direction for y1-axis
   *   bit 5: pulse for     y1-axis
   *   bit 6: direction for x-axis
   *   bit 7: pulse for     x-axis
   */
   DDRB = B00000011;
   /*  bit 0: direction of  z-axis //pin 8
   *   bit 1: pulse of      z-axis //pin 9
   *   bit 2 - 4 : not used
   *   bit 5 : Estop read
   *   bit 6 & 7: cannot be used
   */
   pinMode(inPinEstop,INPUT);//to Estop
   Serial.begin(38400);
}

void loop() {
  //Serial.println(digitalRead(inPinEstop));
  //Serial.println("Hello"); 
  if(!(digitalRead(inPinEstop))) return;

  analogValX  = analogRead(analogPinX);
  analogValYL = analogRead(analogPinYL);
  analogValYR = analogRead(analogPinYR);
  analogValZ  = analogRead(analogPinZ);

  Serial.println("X: "+(String)analogValX + " YL: "+(String)analogValYL + " YR: "+(String)analogValYR + " Z: "+(String)analogValZ);
  //if(analogValX > analogHighVal || analogValYL > analogHighVal || analogValYR > analogHighVal || analogValZ > analogHighVal) return;
  //delay(1000);
  if(analogValX > analogHighVal) movedX = 1;
  if(analogValYL > analogHighVal || analogValYR > analogHighVal) movedY = 1;
  if(analogValZ > analogHighVal) movedZ = 1;
  
  if(movedX == 2) movex = 0;
  if(movedY == 2) movey = 0;
  if(movedZ == 2) movez = 0;

  if(movex){
    if(movedX==0){
      if( checkloopx >= 1000 ){
        checkloopx=0;
        delayMicroseconds(500000);
      }
      checkloopx++;
      // pulse...
      PORTD = B10000000;//move left along X
      delayMicroseconds(spdx);
      PORTD = B00000000;
      delayMicroseconds(spdx);
    }
    if(movedX==1){//move right along X
      PORTD = B11000000;
      delayMicroseconds(spdx);
      PORTD = B01000000;
      delayMicroseconds(spdx);
      movedX = 2;
    }
  }
  if(movey){
    if(movedY==0){
      if( checkloopy >= 1000 ){
        checkloopy=0;
        delayMicroseconds(500000);
      }
      checkloopy++;
      // pulse...
      PORTD = B00111100;
      delayMicroseconds(spdy);
      PORTD = B00010100;
      delayMicroseconds(spdy);
    }
    if(movedY==1){
      PORTD = B00101000;
      delayMicroseconds(spdy);
      PORTD = B00000000;
      delayMicroseconds(spdy);
      movedY = 2;
    }
 }

  if(movez){
    if(movedZ==0){
      if( checkloopzUP >= 100 ){
        checkloopzUP=0;
        delayMicroseconds(500000);
      }  
      checkloopzUP++;
      // pulse...
      PORTB = B00000011;
      delayMicroseconds(spdz);
      PORTB = B00000001;
      delayMicroseconds(spdz);
    }
    if(movedZ==1){
      PORTB = B00000010;
      delayMicroseconds(spdz);
      PORTB = B00000000;
      delayMicroseconds(spdz);
      movedZ = 2;
    }
  }
}
