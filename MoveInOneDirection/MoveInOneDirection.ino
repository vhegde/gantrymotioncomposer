boolean setdirx=HIGH;//LOW is left
boolean setdiry=LOW;//LOW is closer to us
boolean setdirz=LOW;// LOW is the down direction

boolean movex=0;
boolean movey=0;
boolean movez=1;

boolean movedX=0;
boolean movedY=0;
boolean movedZ=0;

int checkloopx=0;
int checkloopy=0;
int checkloopz=0;
int checkloopzDN=0;

int spdx=500;
int spdy=500;
int spdz=1000;

int inPinEstop = 13;    // Estop connected to digital pin 13

int val = 0;      // variable to store the read value
int analogPinX = A0, analogPinYL = A1, analogPinYR = A2, analogPinZ = A3;
int analogValX = -1, analogValYL = -1, analogValYR = -1, analogValZ = -1;
int analogLowVal = 200, analogHighVal = 700;//for 10 bit ADC, high is 1024

void setup() {
  DDRD = B11111111;
  /*   This port is used to control both the direction
   *   and the pulse of 4 separate motors.
   *   bit 0: Not used
   *   bit 1: not used
   *   bit 2: direction of  y2-axis
   *   bit 3: pulse of      y2-axis
   *   bit 4: direction for y1-axis
   *   bit 5: pulse for     y1-axis
   *   bit 6: direction for x-axis
   *   bit 7: pulse for     x-axis
   */
   DDRB = B00000011;
   /*  bit 0: direction of  z-axis //pin 8
   *   bit 1: pulse of      z-axis //pin 9
   *   bit 2 - 4 : not used
   *   bit 5 : Estop read
   *   bit 6 & 7: cannot be used
   */
   pinMode(inPinEstop,INPUT);//to Estop
   Serial.begin(38400);
}

void revmotorx(){
  setdirx=!setdirx;
  delayMicroseconds(10000);
}

void revmotory(){
  setdiry=!setdiry;
  delayMicroseconds(10000);
}

void revmotorz(){
  setdirz=!setdirz;
  delayMicroseconds(10000);
}

void loop() {
  //Serial.println(digitalRead(inPinEstop));
  //Serial.println("Hello"); 
  if(!(digitalRead(inPinEstop))) return;

  analogValX  = analogRead(analogPinX);
  analogValYL = analogRead(analogPinYL);
  analogValYR = analogRead(analogPinYR);
  analogValZ  = analogRead(analogPinZ);
  
  //Serial.println("X: "+(String)analogValX + " YL: "+(String)analogValYL + " YR: "+(String)analogValYR + " Z: "+(String)analogValZ);
  //delay(1000);
  
  if( movex && !movedX){
    if( checkloopx >= 1000 ){
      checkloopx=0;
      delayMicroseconds(500000);
      //revmotorx();
      movedX = 1;
    }
    checkloopx++;
    // pulse...
    if( setdirx ){
      PORTD = B11000000;
      delayMicroseconds(spdx);
      PORTD = B01000000;
      delayMicroseconds(spdx);
    }else{
      PORTD = B10000000;
      delayMicroseconds(spdx);
      PORTD = B00000000;
      delayMicroseconds(spdx);
    }
  }

  if( movey && !movedY){
    if( checkloopy >= 1000 ){
      checkloopy=0;
      delayMicroseconds(500000);
      //revmotory();
      movedY = 1;
    }
    checkloopy++;
    // pulse...
    if( setdiry ){
      PORTD = B00111100;
      delayMicroseconds(spdy);
      PORTD = B00010100;
      delayMicroseconds(spdy);
    }else{
      PORTD = B00101000;
      delayMicroseconds(spdy);
      PORTD = B00000000;
      delayMicroseconds(spdy);
    }
  }

  if( movez && !movedZ){
    if( checkloopz >= 1000 ){
      checkloopz=0;
      delayMicroseconds(500000);
      //revmotorz();
      movedZ = 1;
    }
    checkloopz++;
    // pulse...
    if( setdirz ){
      PORTB = B00000011;
      delayMicroseconds(spdz);
      PORTB = B00000001;
      delayMicroseconds(spdz);
    }else{
      PORTB = B00000010;
      delayMicroseconds(spdz);
      PORTB = B00000000;
      delayMicroseconds(spdz);
    }
  }
}
