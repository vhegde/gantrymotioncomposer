# gantryMotionComposer



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

```
git clone https://gitlab.cern.ch/vhegde/gantrymotioncomposer.git
```

If you're starting fresh, you'll need to install the arduino IDE.  Great instructions
can be found on the arduino websites.  If you're installing the arduino IDE on ubuntu
make sure your user is in the dialout user group (also remember to log out and back
in to make sure that change takes effect).

For serial communication to the board, you'll also want to get the python serial library.

`pip install pyserial`

## Using Gcode to move
Simple Gcode commands can be used to move the gantry. Use interactive python3 using
```
cd GcodeMotion
python3 -i GcodeInterpretter.py
```
To move 1000 steps along X+, Y- direction with delay 100 microsec between steps, relative to current position
```
G91 G1X1000Y-1000Z0F100
```

To move to absolute location (X,Y,Z) = (1000,-1000,0), use `G90` instead of `G91`
```
G90 G1X1000Y-1000Z0F100
```

Once a `G90` or `G91` is used, next commands need not start with these. It will use the previous command.

To home the gantry use 
```
HOME
```

To get the absolute X, Y, Z location and last speed/delay use 
```
GetPosition
``` 
or simply 
```
gps
```

To know the status of limit switches, use 
```
LimSWStatus
```

If the switches are not engaged, then you should get 0 for each of them.

To clode the Gcode session use
```
.G
```

### About the syntax & motion


- Press physical E-stop to stop motion.

- Gcode is not cose sensitive

- To move only along X, Y or Z use only those arguments. If no value is given to F, defalut value is 500. If F value is < 10, then it is taken as 10.

- If there are errors, they are indicated as `E-*` and the error codes can be found in [errorCodes.txt](https://gitlab.cern.ch/vhegde/gantrymotioncomposer/GcodeMotion/errorCodes.txt)

- If there are errors or E-stop is pressed, `HOME` the gantry before making any movement.

- After completing the motion, print out should have `D-000` in it.

- Allowed ranges of steps for safe operation:

| Axis | Minimum | Maximum |
| ------ | ------ | ------ |
| X | 0 | 238000 |
| Y | -159000 | 0 |
| Z | -6500 | 0 |
