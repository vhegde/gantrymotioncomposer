boolean setdirx=HIGH;//LOW is left
boolean setdiry=HIGH;//LOW is closer to us
boolean setdirz=LOW;// LOW is the down direction

int spdx=500;
int spdy=500;
int spdz=1000;

String command;

void setup() {
  DDRD = B11111111;
  /*   This port is used to control both the direction
 *   and the pulse of 4 separate motors.
 *   bit 0: Not used
 *   bit 1: not used
 *   bit 2: direction of  y2-axis
 *   bit 3: pulse of      y2-axis
 *   bit 4: direction for y1-axis
 *   bit 5: pulse for     y1-axis
 *   bit 6: direction for x-axis
 *   bit 7: pulse for     x-axis
 */
 DDRB = B00000011;
 /*  bit 0: direction of  z-axis //pin 8
 *   bit 1: pulse of      z-axis //pin 9
 *   bit 2 - 4 : not used
 *   bit 5 : Estop read
 *   bit 6 & 7: cannot be used
 */
 Serial.begin(250000);
 Serial.setTimeout(1);
}

String fullMsg;
String msgField[2];

void getCSVfields(){
  byte sentencePos =0;
  int commaCount=0;
  msgField[commaCount]="";
  while (sentencePos < fullMsg.length()){
    if(fullMsg.charAt(sentencePos) == ','){
      commaCount++;
      msgField[commaCount]="";
      sentencePos++;
    }
    else{
      msgField[commaCount] += fullMsg.charAt(sentencePos);
      sentencePos++;
    }
  } 
}

void revmotorx(){
  setdirx=!setdirx;
  delayMicroseconds(5);
}

void revmotory(){
  setdiry=!setdiry;
  delayMicroseconds(5);
}

void revmotorz(){
  setdirz=!setdirz;
  delayMicroseconds(5);
}

void movex(int steps){
  if( steps < 0 ){
    setdirx = LOW;
    steps*=-1;
  }
  else setdirx = HIGH;
  
  while( steps > 0 ){
    if( setdirx ){
      PORTD = B11000000;
      delayMicroseconds(spdx);
      PORTD = B01000000;
      delayMicroseconds(spdx);
    }else{
      PORTD = B10000000;
      delayMicroseconds(spdx);
      PORTD = B00000000;
      delayMicroseconds(spdx);
    }
    steps-=1;
  }
}

void movey(int steps){
  if( steps < 0 ){
    setdiry = LOW;
    steps*=-1;
  }
  else setdiry = HIGH;

  while( steps > 0 ){
    if( setdiry ){
      PORTD = B00111100;
      delayMicroseconds(spdy);
      PORTD = B00010100;
      delayMicroseconds(spdy);
    }else{
      PORTD = B00101000;
      delayMicroseconds(spdy);
      PORTD = B00000000;
      delayMicroseconds(spdy);
    }  
    steps-=1;
  }
  
}

void movez(int steps){
  if( steps < 0 ){
    setdirz = LOW;
    steps*=-1;
  }
  else setdirz = HIGH;

  while( steps > 0 ){
    if( setdirz ){
      PORTB = B00000011;
      delayMicroseconds(spdz);
      PORTB = B00000001;
      delayMicroseconds(spdz);
    }else{
      PORTB = B00000010;
      delayMicroseconds(spdz);
      PORTB = B00000000;
      delayMicroseconds(spdz);
    }
    steps-=1;
  }

}

void loop() {
  if( Serial.available() == 16){
    command = Serial.readString();
    Serial.println(command);
    if( command.startsWith("moveX") ){
      int mult = 1;
      command = command.substring(5);
      while( command[0] == '0' ){
        command = command.substring(1);
      }
      Serial.print(command);
      if( command[0] == '-' ){
        Serial.print('n');
        mult = -1;
        command = command.substring(1);
      }
      movex(mult*command.toInt());
      Serial.print(command.toInt());
    }// moveX
    
    if( command.startsWith("moveY") ){
      int mult = 1;
      command = command.substring(5);
      while( command[0] == '0' ){
        command = command.substring(1);
      }
      Serial.print(command);
      if( command[0] == '-' ){
        Serial.print('n');
        mult = -1;
        command = command.substring(1);
      }
      movey(mult*command.toInt());
      Serial.print(command.toInt());
    }// moveY
    
    if( command.startsWith("moveZ") ){
      int mult = 1;
      command = command.substring(5);
      while( command[0] == '0' ){
        command = command.substring(1);
      }
      Serial.print(command);
      if( command[0] == '-' ){
        Serial.print('n');
        mult = -1;
        command = command.substring(1);
      }
      movez(mult*command.toInt());
      Serial.print(command.toInt());
    }// moveZ
 
//    if( command.startsWith("displ") ){
//      //Serial.print(command);
//      command = command.substring(5);
//      fullMsg = command;
//      getCSVfields();
//      //Serial.print(msgField[0]);
//      //Serial.print(msgField[1]);
//      int dispx = msgField[0].toInt();
//      int dispy = msgField[1].toInt();
//      int x10 = msgField[0]/10; 
//      int y10 = msgField[1]/10;
//      while( msgField[0] && msgField[1] > 10 ){
//        movex(
//      }
//    }
//    
  }// Serial.available()
  command="";
}
