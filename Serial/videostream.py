import cv2
import numpy as np

def play():
     camera = cv2.VideoCapture(2)
     camera.set(cv2.CAP_PROP_AUTOFOCUS, 0) # turn the autofocus off
     camera.set(cv2.CAP_PROP_FRAME_WIDTH, 4096)
     camera.set(cv2.CAP_PROP_FRAME_HEIGHT, 2160)
     # camera.set(cv2.DEFAULT_V4L_WIDTH, 4024) # turn the autofocus off
     # camera.set(cv2.DEFAULT_V4L_HEIGHT, 3018) # turn the autofocus off 
     
     while(camera.isOpened()):
          ret, frame = camera.read()
          print(frame.shape)
          if ret == True:
               cv2.imshow('Frame',frame)
               if cv2.waitKey(1) == ord('q'):
                    break
          else:
               break
     camera.release()
     cv2.destroyAllWindows()
